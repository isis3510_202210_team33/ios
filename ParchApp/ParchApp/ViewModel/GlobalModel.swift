//
//  GlobalModel.swift
//  ParchApp
//
//  Created by Simon Rendon Arango on 25/03/22.
//

import Foundation
import SwiftUI
class GlobalModel: ObservableObject {

    @Published var loggedIn :Bool = false
    @Published var userId : Int = 0
    @Published var landscape : Bool = UIDevice.current.orientation.isLandscape
    @Published var isActive : Bool = false
    @Published var orientation = UIDeviceOrientation.unknown
    
    func activate() {
        isActive = true
    }
    func changeOrientation(newOrientation : UIDeviceOrientation){
        orientation = newOrientation
    }

}
