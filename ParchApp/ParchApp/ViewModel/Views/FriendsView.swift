//
//  FriendsView.swift
//  ParchApp
//
//  Created by Simon Rendon Arango on 24/03/22.
//

import SwiftUI

struct FriendsView: View {
    @State var pressed: String = "None"
    @State var searchText: String = ""
    @State var showingGroup = false
    
    var body: some View {
        GeometryReader{metrics in
            ZStack{
                VStack {
                    TopBarView().frame(width: metrics.size.width, height: metrics.size.height*0.1)
                    HStack{
                        Spacer()
                        ZStack{
                            Color.secondaryColor
                            Button(action:pressFriends){
                                VStack (alignment: .center){

                                    Text("Amigos")
                                        .foregroundColor(Color.wht)
                                        .fontWeight(.heavy)
                                        .font(.system(size: 18))
                                    Image(systemName: "person.fill")
                                        .resizable()
                                        .scaledToFit()
                                        .frame(width: metrics.size.width*0.15, height: metrics.size.width*0.15 )
                                        .foregroundColor(Color.wht)
                                    Text("0")
                                        .foregroundColor(Color.wht)
                                        .fontWeight(.heavy)
                                        .font(.system(size: 18))
                                }
                            }
                        }.cornerRadius(20).frame(width: metrics.size.width*0.45, height: metrics.size.width*0.45)

                        Spacer()
                        ZStack{
                            Color.secondaryColor
                            Button(action:pressGroups){
                            VStack (alignment: .center){
                                Text("Grupos")
                                    .foregroundColor(Color.wht)
                                    .fontWeight(.heavy)
                                    .font(.system(size: 18))
                                Image(systemName: "person.3.fill")
                                    .resizable()
                                    .scaledToFit()
                                    .frame(width: metrics.size.width*0.25, height: metrics.size.width*0.15 )
                                    .foregroundColor(Color.wht)
                                Text("5")
                                    .foregroundColor(Color.wht)
                                    .fontWeight(.heavy)
                                    .font(.system(size: 18))
                            }
                            }
                        }.cornerRadius(20).frame(width: metrics.size.width*0.45, height: metrics.size.width*0.45)
                        Spacer()
                    }.padding(.bottom, 10)
                    if pressed == "None"{
                        Text("Buscar Amigos")
                            .fontWeight(.heavy).foregroundColor(Color.blk).font(.system(size: 22))
                        SearchBar(text: $searchText)
                            .padding(.top,0)

                        //List(todoItems.filter({ searchText.isEmpty ? true : $0.name.contains(searchText) })) { item in
                        //    Text(item.name)
                        //}

//                        List{
//                            ForEach(0..<10){i in
//                                HStack{
//
//                                    Text("@user\(i)")
//
//                                    Image(systemName: "person.fill")
//                                    Spacer()
//                                    Text("Añadir").foregroundColor(Color.secondaryColor)
//                                }
//
//
//                            }
//                        }
                    } else if pressed == "Friends" {
                        HStack{
                                                 
                                                AmigosView()
                                                   }
                    }else if pressed == "Groups" {
                        HStack{
                                                 
                                                GroupsView()
                                                   }
                       
                                               }
                    }

                }
            }

        }
    
    
    func pressFriends(){
        if pressed == "None" || pressed == "Groups" {
            pressed = "Friends"
        }else if pressed == "Friends"{
            pressed = "None"
        }
        
        
    }
    func pressGroups(){
        if pressed == "None" || pressed == "Friends" {
            pressed = "Groups"
        }else if pressed == "Groups"{
            pressed = "None"
        }
    }
    func unpress(){
        pressed = "None"
    }
    func verGrupo(){
        self.showingGroup.toggle()
    }
}

