//
//  MotherView.swift
//  AuthenticationStarter
//
//  Created by Work on 13.12.21.
//

import SwiftUI

struct MotherView: View {
    
    @EnvironmentObject var viewRouter: ViewRouter
    @ObservedObject var global = GlobalModel()
    
    let defaults = UserDefaults.standard
    
    
    
    var body: some View {
        let logged = defaults.bool(forKey: "userLoggedIn")
        if !logged {
            switch viewRouter.currentPage {
            case .signUpPage:
                SignUpView()
            case .signInPage:
                SignInView()
            case .homePage:
                MainView(global: global)
            }
        }
        else{
            MainView(global: global)
        }
    }
}

struct MotherView_Previews: PreviewProvider {
    static var previews: some View {
        MotherView().environmentObject(ViewRouter())
    }
}
