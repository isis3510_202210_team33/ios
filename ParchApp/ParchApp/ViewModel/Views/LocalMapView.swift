//
//  LocalMapView.swift
//  ParchApp
//
//  Created by Federico Gadea on 24/03/22.
//

import SwiftUI
import MapKit

struct Marker: Identifiable {
    let id = UUID()
    var location: MapMarker
}
struct LocalMapView: View {
    
    var lugar : LugarModel
        init(lugar: LugarModel) {
            self.lugar = lugar
        }
    
    @StateObject private var favViewModel = FavoriteViewModel()
    
    var body: some View {
        
        let markers = [Marker(location: MapMarker(coordinate: CLLocationCoordinate2D(latitude: Double(lugar.latitud)!/10, longitude: Double(lugar.longitud)!/10), tint: .red))]
        
        
        GeometryReader{metrics in
            VStack {
                TopBarView().frame(width: metrics.size.width, height: metrics.size.height*0.1)
                        VStack{
                            Map(coordinateRegion: $favViewModel.region, showsUserLocation: true, annotationItems: markers){ marker in
                                marker.location
                            }
                                 .ignoresSafeArea()
                                 .accentColor(Color.primaryColor)
                                 .onAppear{
                                     favViewModel.test(latitud: Double(lugar.latitud)!/10, longitud: Double(lugar.longitud)!/10)
                                 }
                        }

                    VStack{
                            Image(systemName: "heart")
                                  .resizable()
                                  .scaledToFit()
                                  .frame(width: metrics.size.width*0.08, height: metrics.size.width*0.08)
                              
                            Image(systemName: "square.and.arrow.up")
                                  .resizable()
                                  .scaledToFit()
                                  .frame(width: metrics.size.width*0.08, height: metrics.size.width*0.08)
                          }
                          VStack{
                              Text(lugar.nombre)
                                  .fontWeight(.bold)
                                  .minimumScaleFactor(0.01)
                              Image(systemName: "photo")
                                  .resizable()
                                  .scaledToFit()
                                  .frame(width: metrics.size.width*0.18, height: metrics.size.width*0.18)
                              
                              Text("#/10")
                                  .foregroundColor(Color.secondaryColor)
                          }
                          VStack{
                              Text("Dirección").foregroundColor(Color.secondaryColor)
                              Text(lugar.direccion)
                              Text("Precio").foregroundColor(Color.secondaryColor)
                              Text("20k")
                          }
                          Spacer()
                      }
                  }.cornerRadius(10)
                  Spacer()
              }
          }



