//
//  GroupsView.swift
//  ParchApp
//
//  Created by Federico Gadea on 1/04/22.
//

import SwiftUI

struct GroupsView: View {
    @State var presentPopup = false
    @State var grupos : [GrupoModel] = []
    @State var gruposS : ArraySlice<GrupoModel> = []
    @ObservedObject var monitor = NetworkMonitor()
    
    var body: some View {
        GeometryReader{metrics in
            VStack {
                
                ScrollView {
                    VStack{
                            ForEach(grupos){grupo in
                            GroupView (grupo: grupo).frame(width: metrics.size.width, height: metrics.size.height)
                            }
                    }
                }
            }
        }.onAppear(perform: {
            if (self.monitor.isConnected){
                let grupos = HTTPRequest()
                grupos.getGrupos() { lista in
                    self.grupos = lista
                }
                print(self.grupos)
                print(self.gruposS)
            }
        })
        .alert(isPresented: self.$monitor.notConnected) {
            Alert (title: Text("No Internet Connection"),
                   message: Text("Please enable Wifi or Celluar data"),
                   primaryButton: .default(Text("Settings"), action: {
                UIApplication.shared.open(URL(string: UIApplication.openSettingsURLString)!)
            }),
            secondaryButton: .default(Text("Cancel"))
            )
        }
    }
}

