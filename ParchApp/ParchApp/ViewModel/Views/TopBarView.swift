//
//  TopBarView.swift
//  ParchApp
//
//  Created by Simon Rendon Arango on 23/03/22.
//

import SwiftUI

struct TopBarView: View {
    var body: some View {
        GeometryReader{metrics in
            ZStack {
                Color.primaryColor.ignoresSafeArea(.all)
                Color.wht
                ZStack{
                    Color.primaryColor.ignoresSafeArea(.all)
                    VStack (alignment: .leading, spacing: 20){
                        HStack(alignment: .center, spacing: 10){
                            Spacer()
                            TitleLogoView()
                                
                            Image("Logo")
                                .resizable()
                                .scaledToFit()
                                .padding(10)
                                .frame(width:  metrics.size.width*0.12, height:  metrics.size.width*0.12, alignment: .trailing)
                                .foregroundColor(Color.white)
                                .font(Font.custom("Righteous-Regular", size: 500))
                                .minimumScaleFactor(0.01)
                                .shadow(color: Color.black.opacity(0.6), radius: 1, x: 2, y: 2)
                            
                            Spacer()
                        }
                    }
                    
                }.cornerRadius(20, corners: [.bottomLeft, .bottomRight])
            }
        }
        
        
    }
}

struct TopBarView_Previews: PreviewProvider {
    static var previews: some View {
        TopBarView()
    }
}
