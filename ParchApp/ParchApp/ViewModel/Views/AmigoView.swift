//
//  AmigoView.swift
//  ParchApp
//
//  Created by Federico Gadea on 7/05/22.
//

import SwiftUI

struct AmigoView: View {
    @State private var isShareSheetShowing = false
    
    var amigo : ClienteModel
        init(amigo: ClienteModel) {
            self.amigo = amigo
        }
    
    var body: some View {
      GeometryReader{metrics in
            HStack{
                Spacer()
                ZStack{
                    Color.secondaryColor.opacity(0.2)
                    HStack{
                        Spacer()
                        VStack{
                            Image("PersonPlaceholder")
                                .resizable()
                                .scaledToFit()
                                .frame(width: metrics.size.width*0.15, height: metrics.size.width*0.1)
                                .clipShape(Circle())
                                .overlay(Circle().stroke(Color.secondaryColor, lineWidth: 3))
                        }
                        VStack{
                            Text(amigo.nombre)
                                .fontWeight(.bold)
                            Text("@"+amigo.username).foregroundColor(Color.secondaryColor)
                            
                        }.frame(width: metrics.size.width*0.5)
                        VStack{
                            Text(amigo.correo)
                                .fontWeight(.bold)
                                .minimumScaleFactor(0.01)
                            Image(systemName: "photo")
                                .resizable()
                                .scaledToFit()
                                .frame(width: metrics.size.width*0.18, height: metrics.size.width*0.18)
                            
                            Text("#/10")
                                .foregroundColor(Color.secondaryColor)
                        }
                        Spacer()
                    }
                }.cornerRadius(10)
                
                Spacer()
            }
        }
        
    }
}


