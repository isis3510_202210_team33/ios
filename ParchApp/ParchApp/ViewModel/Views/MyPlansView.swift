//
//  MyPlansView.swift
//  ParchApp
//
//  Created by Simon Rendon Arango on 24/03/22.
//

import SwiftUI
import Combine


struct MyPlansView: View {
    
    @State var selectedDate : Date = Date.now
    @State var selectedBudget : String = ""
    @State private var date = Date()
    @FocusState var numberpad :Bool
    @State var showingReco = false
    @State var planes : [PlanModel] = []
    @ObservedObject var monitor = NetworkMonitor()
    
    enum Grupos: String, CaseIterable, Identifiable {
        case G1, G2, G3,G4,G5
        var id: Self { self }
    }
    
    
    @State private var selectedGroup: Grupos = .G1
    
    var body: some View {
        GeometryReader{metrics in
            ZStack{
                VStack {
                    TopBarView().frame(width: metrics.size.width, height: metrics.size.height*0.1)
                    ScrollView {
                        VStack{
                            ForEach(planes) {plan in
                                PlanView(plan: plan).frame(width: metrics.size.width*0.9, height: metrics.size.height*0.15).cornerRadius(20)
                            }
                        }
                    }
                    ZStack{
                        Color.wht.opacity(0.3)
                        
                        VStack {
                            
                            Text("Nuevo Plan").fontWeight(.black).font(.system(size: 24))
                            HStack{
                                Spacer()
                                VStack{
                                    ScrollView{
                                        
                                        HStack{
                                            Text("Grupo:")
                                            Picker("Grupo de amigos", selection: $selectedGroup){
                                                
                                                ForEach(Grupos.allCases) { grupo in
                                                    
                                                    Text(grupo.rawValue.capitalized)
                                                }
                                            }
                                        }
                                        DatePicker(
                                            "Fecha:",
                                            selection: $date,
                                            displayedComponents: [.date,.hourAndMinute]
                                        )
                                        
                                        HStack {
                                            Text("Presupuesto:")
                                            
                                            TextField("Presupuesto por persona", text: $selectedBudget).keyboardType(.numberPad) .focused($numberpad).submitLabel(.done)
                                                .onReceive(Just(selectedBudget)) { newValue in
                                                    let filtered = newValue.filter { "0123456789".contains($0) }
                                                    if filtered != newValue {
                                                        self.selectedBudget = filtered
                                                    }
                                                }
                                        
                                            Button("Listo") {
                                                numberpad = false
                                            }
                                        }
                                    }
                                    HStack{
                                        Button(action: {self.showingReco.toggle()}){
                                            Image(systemName: "plus.app.fill").resizable().scaledToFit().frame(width: metrics.size.width*0.1)
                                            Text("Planes recomendados").sheet(isPresented: $showingReco) {
                                                RecommendedView().foregroundColor(Color.blk)
                                            }
                                        }
                                        Spacer()
                                    }
                                    Spacer()
                                    HStack{
                                        Button(action: {}){
                                            Image(systemName: "checkmark.circle.fill").resizable().scaledToFit().frame(width: metrics.size.width*0.1)
                                            Text("Crear")
                                        }
                                        Spacer()
                                    }
                                    Spacer()
                                }
                            }
                        }.frame(width: metrics.size.width*0.9, height: metrics.size.height*0.5)
                        
                        
                    }
                }
                
            }.onAppear(perform: {
                if (self.monitor.isConnected){
                let planes = HTTPRequest()
                planes.getPlanes { lista in
                    self.planes = lista
                }
                }
            })
            .alert(isPresented: self.$monitor.notConnected) {
                Alert (title: Text("No Internet Connection"),
                       message: Text("Please enable Wifi or Celluar data"),
                       primaryButton: .default(Text("Settings"), action: {
                    UIApplication.shared.open(URL(string: UIApplication.openSettingsURLString)!)
                }),
                secondaryButton: .default(Text("Cancel"))
                )
            }
        }
    }
    
}
