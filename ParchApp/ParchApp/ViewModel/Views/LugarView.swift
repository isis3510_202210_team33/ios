//
//  LugarView.swift
//  ParchApp
//
//  Created by Federico Gadea on 24/03/22.
//

import SwiftUI

struct LugarView: View {
    @State var showingFav = false
    var lugar : LugarModel
        init(lugar: LugarModel) {
            self.lugar = lugar
        }
    
    var body: some View {
        
      GeometryReader{metrics in
            HStack{
                Spacer()
                ZStack{
                    Color.secondaryColor.opacity(0.2)
                    HStack{
                        Spacer()
                        VStack{
                                Image(systemName: "heart")
                                      .resizable()
                                      .scaledToFit()
                                      .frame(width: metrics.size.width*0.08, height: metrics.size.width*0.08)
                                  
                                Image(systemName: "square.and.arrow.up")
                                      .resizable()
                                      .scaledToFit()
                                      .frame(width: metrics.size.width*0.08, height: metrics.size.width*0.08)      
                              }
                              VStack{
                                  Text(lugar.nombre)
                                      .fontWeight(.bold)
                                      .minimumScaleFactor(0.01)
                                  Image(systemName: "photo")
                                      .resizable()
                                      .scaledToFit()
                                      .frame(width: metrics.size.width*0.18, height: metrics.size.width*0.18)
                                  
                                  Text("#/10")
                                      .foregroundColor(Color.secondaryColor)
                              }
                              VStack{
                                          Text("Dirección").foregroundColor(Color.secondaryColor)
                                  Text(lugar.direccion)
                                          Button(action: showFav){
                                              ZStack {
                                                Text("Ver ubicacion").foregroundColor(Color.blue)
                                              }.sheet(isPresented: $showingFav) {
                                                  LocalMapView(lugar: lugar).foregroundColor(Color.blue)}
                                          }
                                          Text("Precio").foregroundColor(Color.secondaryColor)
                                          Text("20k")
                                  }
                        Spacer()
                              }
                        
                          }
                Spacer()
                      }.cornerRadius(10)
                      
                      
                  }
              }
    func showFav(){
        self.showingFav.toggle()
        }
}
