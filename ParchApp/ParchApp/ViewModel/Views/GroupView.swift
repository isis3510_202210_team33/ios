//
//  GroupView.swift
//  ParchApp
//
//  Created by Simon Rendon Arango on 24/03/22.
//

import SwiftUI

struct GroupView: View {
    var grupo : GrupoModel
        init(grupo: GrupoModel) {
            self.grupo = grupo
        }
    
    var body: some View {
        GeometryReader{metrics in
            HStack {
                Spacer()
                VStack{
                    Spacer()
                    Text(grupo.nombre).fontWeight(.bold).font(.system(size: 40))
                    Spacer()
                    HStack{
                        ForEach(0..<10){j in
                            Image(systemName: "person.fill").font(.system(size: 25))
                        }
                    }
                    Spacer()
                   // PlanView().frame(width: metrics.size.width*0.9, height: metrics.size.height*0.2).cornerRadius(20)
                    Spacer()
                    Text("\(grupo.balance)").fontWeight(.bold).font(.system(size: 32))
                    List{
                        ForEach(0..<2){i in
                            HStack{
                                Text("Persona \(i)")
                                Image(systemName: "person.fill")
                                Spacer()
                                Text("20K").foregroundColor(Color.green)
                            }
                        }
                        ForEach(2..<4){i in
                            HStack{
                                Text("Persona \(i)")
                                Image(systemName: "person.fill")
                                Spacer()
                                Text("-20K").foregroundColor(Color.red)
                            }
                        }
                    }
                }
                Spacer()
            }
        }
    }
}
