//
//  FavoriteView.swift
//  ParchApp
//
//  Created by Federico Gadea on 24/03/22.
//

import SwiftUI

struct FavoriteView: View {
    @State var presentPopup = false
    @State var lugares : [LugarModel] = []
    @ObservedObject var monitor = NetworkMonitor()
    
    var body: some View {
        GeometryReader{metrics in
            VStack {
                TopBarView().frame(width: metrics.size.width, height: metrics.size.height*0.1)
                ScrollView {
                        VStack{
                            ForEach(lugares) {lugar in
                                LugarView (lugar: lugar).frame(width: metrics.size.width, height: metrics.size.height*0.2)
                        }
                    }
                }
            }
        }.onAppear(perform: {
            if(monitor.isConnected){
            let lugares = HTTPRequest()
            lugares.getLugares() { lista in
                self.lugares = lista
            }
            }
        })
        .alert(isPresented: self.$monitor.notConnected) {
            Alert (title: Text("No Internet Connection"),
                   message: Text("Please enable Wifi or Celluar data"),
                   primaryButton: .default(Text("Settings"), action: {
                UIApplication.shared.open(URL(string: UIApplication.openSettingsURLString)!)
            }),
            secondaryButton: .default(Text("Cancel"))
            )
        }
    }
}

struct FavoriteView_Previews: PreviewProvider {
    static var previews: some View {
        FavoriteView()
    }
}
