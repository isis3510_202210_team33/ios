//
//  RecommendedView.swift
//  ParchApp
//
//  Created by Simon Rendon Arango on 25/03/22.
//

import SwiftUI

struct RecommendedView: View {
    var body: some View {
        GeometryReader{ metrics in
            VStack{
                Spacer()
                ZStack {
                    Color.secondaryColor
                    Button(action:{}){
                        HStack{
                            Image(systemName: "shuffle").foregroundColor(Color.white)
                            Text("Aleatorio").foregroundColor(Color.white)
                        }
                       
                        
                    }.frame(width: metrics.size.width*0.8, height: metrics.size.height*0.2)
                }.frame(width: metrics.size.width*0.8, height: metrics.size.height*0.2).cornerRadius(20)
                Spacer()
                List{
                    ForEach(0..<10){i in
                        Button(action:{}){
                        Text("Recomendado \(i)")
                        }
                    }
                }
                Spacer()
            }
        }
        
    }
}

struct RecommendedView_Previews: PreviewProvider {
    static var previews: some View {
        RecommendedView()
    }
}
