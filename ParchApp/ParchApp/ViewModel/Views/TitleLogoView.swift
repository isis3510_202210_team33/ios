//
//  TitleLogoView.swift
//  ParchApp
//
//  Created by Simon Rendon Arango on 23/03/22.
//

import SwiftUI

struct TitleLogoView: View {
    var body: some View {
        GeometryReader { metrics in
            ZStack{
                Color.primaryColor.ignoresSafeArea(.all)
                Text("PARCHAPP")
                    .foregroundColor(Color.white)
                    .font(Font.custom("Righteous-Regular", size: 500))
                    .minimumScaleFactor(0.01)
                    .shadow(color: Color.black.opacity(0.6), radius: 1, x: 2, y: 2)
            }
        }
    }
}

struct TitleLogoView_Previews: PreviewProvider {
    static var previews: some View {
        TitleLogoView()
    }
}

