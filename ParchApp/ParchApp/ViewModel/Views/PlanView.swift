//
//  PlanView.swift
//  ParchApp
//
//  Created by Simon Rendon Arango on 24/03/22.
//

import SwiftUI

struct PlanView: View {
    var imageUrl = URL(string: "https://sostenibleosustentable.com/es/wp-content/uploads/2021/01/parques-nacionales-300x300.jpg")
    
    var plan : PlanModel
        init(plan: PlanModel) {
            self.plan = plan
        }
    
    var body: some View {
        GeometryReader{metrics in
            ZStack{
                Color.secondaryColor.opacity(0.1)


                HStack{
                    Spacer()

                    VStack{
                        Text(plan.descripcion).fontWeight(.black)
                        Text(plan.horaInicia)
                    }
                    Spacer()

                    
                    VStack{

                        AsyncImage(url: imageUrl){image in
                            image.resizable()
                        } placeholder: {
                            ProgressView()
                        }.scaledToFit().frame(width: metrics.size.width*0.20)
                        
                    }
                    Spacer()

                    HStack{
                        
                        Image(systemName: "person.crop.circle.fill.badge.checkmark").resizable().scaledToFit().frame(width: metrics.size.width*0.20)
                        Text("5").fontWeight(.black)
                    }
                    Spacer()
                    
                    
                }
            }
        }
    }
}
