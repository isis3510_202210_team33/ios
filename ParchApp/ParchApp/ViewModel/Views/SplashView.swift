//
//  SplashView.swift
//  ParchApp
//
//  Created by Simon Rendon Arango on 23/03/22.
//

import SwiftUI
extension View {
    func Print(_ vars: Any...) -> some View {
        for v in vars { print(v) }
        return EmptyView()
    }
}

struct SplashView: View {

    @ObservedObject var global = GlobalModel()
    @ObservedObject var monitor = NetworkMonitor()
    @ObservedObject var splashVM = SplashViewModel()
    @EnvironmentObject var viewRouter: ViewRouter
    @State var clientes : [ClienteModel] = []
    let defaults = UserDefaults.standard

 
    var body: some View {
        
        GeometryReader{metrics in
            ZStack {
                
                VStack( spacing: 0){
                    Group {
                        
                        if splashVM.orientation.isLandscape {
                            ZStack{
                                Color.primaryColor.ignoresSafeArea()
                                VStack{
                                    
                                    TitleLogoView()
                                    Text("Por favor vuelva a la orientación vertical")
                                }
                            }
                            
                        }else{
                            if splashVM.isActive {
                                MotherView().environmentObject(viewRouter)

                            } else {
                                ZStack{
                                    Color.primaryColor.ignoresSafeArea(.all)
                                    
                                    TitleLogoView().frame(width: metrics.size.width, height: metrics.size.height*0.1)
                                    Print("Hello")
                                }.onAppear {
                                    
                                    print("Hey")
                                    print(monitor.isConnected)
                                    let email = defaults.string(forKey: "userEmail")
                                    if (monitor.isConnected){
                                        DispatchQueue.global(qos: .userInteractive).sync {
                                            if defaults.string(forKey: "userEmail") != nil {
                                                loadCliente(email: defaults.string(forKey: "userEmail")!)
                                            }
                                            withAnimation {
                                                splashVM.activate()
                                            }
                                        }
                                    }
                                    
                                }
                                
                            }
                        }
                        
                    }.onRotate { newOrientation in
                        splashVM.changeOrientation(newOrientation: newOrientation)
                    }
                    
                }
            }
            
        }
    }
    func loadCliente(email: String){
        
        print("load1")
            if MyVariables.cacheCliente[email] != nil {
                let client = MyVariables.cacheCliente.value(forKey: email)!
                print(client)
                MyVariables.cliente = client
                print("was cached")
                print("load2")
              return
            }
        print("load3")
            let cliente = HTTPRequest()
            cliente.getCliente(){lista in
                print("load4")
                self.clientes.append(lista)
                let toCache = self.clientes[0]
                MyVariables.cliente = toCache
                print(MyVariables.cacheCliente.value(forKey: email) as Any)
                MyVariables.cacheCliente.insert(self.clientes[0], forKey: email)
                print(toCache)
                print("was retrieved")
        }
        print("load5")
        
    }
}

struct SplashView_Previews: PreviewProvider {
    static var previews: some View {
        Group {
            SplashView()
        }
    }
}
