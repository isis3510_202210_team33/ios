//
//  ProfileView.swift
//  ParchApp
//
//  Created by Simon Rendon Arango on 23/03/22.
//

import SwiftUI
import Firebase



struct ProfileView: View {
    @ObservedObject var global : GlobalModel
    @EnvironmentObject var viewRouter: ViewRouter
    @State var signOutProcessing = false
    @State var clientes : [ClienteModel] = []
    
    @State var showingFav = false
    
    @State var amigos = 0
    @State var fechaNacimiento = "cargando"
    @State var nombre = "cargando"
    @State var username = "cargando"
    let imageUrl = URL(string: "https://journalismcourses.org/wp-content/uploads/2022/01/Federico-Kukso.png")
    let defaults = UserDefaults.standard
    var profilePic : Image
    let dateFormatter = DateFormatter()
    

    
    
    
    var body: some View {
        GeometryReader{metrics in
            ZStack{
                Color.primaryColor
                VStack{
                    
                    VStack (alignment: .leading, spacing: 20){
                        HStack(alignment: .center, spacing: 10){
                            Spacer()
                            TitleLogoView()
                            
                            Image("Logo")
                                .resizable()
                                .scaledToFit()
                                .padding(10)
                                .frame(width:  metrics.size.width*0.12, height:  metrics.size.width*0.12, alignment: .trailing)
                                .foregroundColor(Color.white)
                                .minimumScaleFactor(0.01)
                                .shadow(color: Color.black.opacity(0.6), radius: 1, x: 2, y: 2)
                            
                            Spacer()
                        }
                        
                        HStack{
                            
                            AsyncImage(url: imageUrl){image in
                                image.resizable()
                                
                                    .padding(0)
                                    .clipShape(Circle())
                                    .overlay(Circle().stroke(Color.secondaryColor, lineWidth: 3))
                            } placeholder: {
                                ProgressView()
                            }.scaledToFit().frame(width: metrics.size.width*0.4)
                            
                            
                            
                            
                            
                            Spacer()
                            Button(action: logOut ){
                                Image(systemName: "rectangle.portrait.and.arrow.right.fill").resizable().scaledToFit().frame(width: metrics.size.width*0.15, height: metrics.size.width*0.15).foregroundColor(Color.white)
                                
                                Text("Cerrar sesion").foregroundColor(Color.white).fontWeight(.bold).frame(width: 60)
                            }
                            Spacer()
                        }
                        
                        Spacer()
                        
                        HStack{
                            Spacer()
                            VStack (alignment: .leading){
                               
                                    Text("Nombre").fontWeight(.bold).foregroundColor(Color.white)
                                    Text(nombre).foregroundColor(Color.white)
                                    Text("Nombre de usuario").fontWeight(.bold).foregroundColor(Color.white)
                                    Text(username).foregroundColor(Color.white)
                                    Text("Fecha de nacimiento").fontWeight(.bold).foregroundColor(Color.white)
                                    Text(fechaNacimiento).foregroundColor(Color.white)
                                
                              
                                
                                
                          
                            }.onAppear(perform: {
                                if MyVariables.cliente.nombre != "aszxcvfbgn" {
                                print("fecha")
                                print( MyVariables.cliente.fechaNacimiento)
                                dateFormatter.dateFormat = "YYYY/MM/dd"
                                fechaNacimiento = (dateFormatter.string(from: MyVariables.cliente.fechaNacimiento))
                                nombre = MyVariables.cliente.nombre
                                username = MyVariables.cliente.username
                                }
                                else{
                                    print("load again")
                                    loadCliente(email:defaults.string(forKey: "userEmail")!)
                                    let delay = DispatchTime.now() + .seconds(8)
                                    DispatchQueue.main.asyncAfter(deadline: delay) {
                                        dateFormatter.dateFormat = "YYYY/MM/dd"
                                        fechaNacimiento = (dateFormatter.string(from: MyVariables.cliente.fechaNacimiento))
                                        nombre = MyVariables.cliente.nombre
                                        username = MyVariables.cliente.username
                                    }
                                    
                                }
                            })
                            Spacer()
                            VStack ( spacing: 0){
                                Image(systemName: "calendar.badge.plus")
                                    .resizable()
                                    .scaledToFit()
                                    .frame(width: metrics.size.width*0.3, height: metrics.size.width*0.3 )
                                    .foregroundColor(Color.white)
                                
                                Text("Importar Calendario")
                                    .foregroundColor(Color.white)
                                    .fontWeight(.heavy)
                                    .font(.system(size: 20))
                                    .minimumScaleFactor(0.9)
                                
                            }
                            Spacer()
                            
                        }
                        
                    }.padding(.bottom, 40)
                    Spacer()
                    HStack{
                        Spacer()
                        ZStack{
                            Color.wht
                            VStack (alignment: .center){
                                Text("Amigos")
                                    .foregroundColor(Color.secondaryColor)
                                    .fontWeight(.heavy)
                                    .font(.system(size: 18))
                                Image(systemName: "person.fill")
                                    .resizable()
                                    .scaledToFit()
                                    .frame(width: metrics.size.width*0.15, height: metrics.size.width*0.15 )
                                    .foregroundColor(Color.secondaryColor)
                                Text("\(amigos)")
                                    .foregroundColor(Color.secondaryColor)
                                    .fontWeight(.heavy)
                                    .font(.system(size: 18))
                            }
                        }.cornerRadius(20).frame(width: metrics.size.width*0.45, height: metrics.size.width*0.45)
                        
                        Spacer()
                        ZStack{
                            Color.wht
                            VStack (alignment: .center){
                                Text("Grupos")
                                    .foregroundColor(Color.secondaryColor)
                                    .fontWeight(.heavy)
                                    .font(.system(size: 18))
                                Image(systemName: "person.3.fill")
                                    .resizable()
                                    .scaledToFit()
                                    .frame(width: metrics.size.width*0.25, height: metrics.size.width*0.15 )
                                    .foregroundColor(Color.secondaryColor)
                                Text("5")
                                    .foregroundColor(Color.secondaryColor)
                                    .fontWeight(.heavy)
                                    .font(.system(size: 18))
                            }
                        }.cornerRadius(20).frame(width: metrics.size.width*0.45, height: metrics.size.width*0.45)
                        Spacer()
                    }.padding(.bottom, 10)
                    Spacer()
                    Button(action: showFav){
                        ZStack {
                            Color.wht
                            
                            HStack{
                                Image(systemName: "heart.fill")
                                    .foregroundColor(Color.secondaryColor)
                                Text("Favoritos")
                                    .foregroundColor(Color.secondaryColor)
                            }
                        }.sheet(isPresented: $showingFav) {
                            FavoriteView().foregroundColor(Color.blk)
                        }
                    }.cornerRadius(20).frame(width: metrics.size.width*0.9, height: metrics.size.height*0.1).padding(.bottom,10)
                    Spacer()
                }
            }.cornerRadius(30).frame(height: metrics.size.height*0.99)
        }
    }
    
    
    func logOut(){
        global.loggedIn=false
        signOutProcessing = true
        let firebaseAuth = Auth.auth()
        do {
            try firebaseAuth.signOut()
        } catch let signOutError as NSError {
            print("Error signing out: %@", signOutError)
            signOutProcessing = false
        }
        defaults.set("", forKey: "userEmail")
        defaults.set(false, forKey: "userLoggedIn")
        MyVariables.cacheCliente = Cache<String, ClienteModel>()
        MyVariables.cliente = ClienteModel(id: 10000, username: "username", nombre: "aszxcvfbgn", password: "password", correo: "correo", fechaNacimiento: Date.now, telefono: 12345678, grupos: [], amigos: [], favoritos: [], intereses: [])

        withAnimation {
            viewRouter.currentPage = .signInPage
        }
    }
    
    func showFav(){
        self.showingFav.toggle()
    }
    
    func loadCliente(email: String){
        if MyVariables.cacheCliente[email] != nil{
            let client = MyVariables.cacheCliente.value(forKey: email)
            print(client!)
            print("was cached")
            return
        }
        
        let cliente = HTTPRequest()
        cliente.getCliente(){lista in
            self.clientes.append(lista)
            let toCache = self.clientes[0]
            MyVariables.cliente = toCache
            print(MyVariables.cacheCliente.value(forKey: email) as Any)
            MyVariables.cacheCliente.insert(self.clientes[0], forKey: email)
            print(toCache)
            print("was retrieved")
        }
        
        
    }
}

