//
//  ContentView.swift
//  ParchApp
//
//  Created by Simon Rendon Arango on 22/03/22.
//

import SwiftUI



extension Color{
    static let primaryColor = Color("PrimaryColor")
    static let secondaryColor = Color("SecondaryColor")
    static let blk = Color("Black")
    static let wht = Color("White")
}

extension View {
    func cornerRadius(_ radius: CGFloat, corners: UIRectCorner) -> some View {
        clipShape( RoundedCorner(radius: radius, corners: corners) )
    }
    func onRotate(perform action: @escaping (UIDeviceOrientation) -> Void) -> some View {
                self.modifier(DeviceRotationViewModifier(action: action))
            }
}

struct ContentView: View {
    
    @ObservedObject var global = GlobalModel()
    init() {
        UITabBar.appearance().backgroundColor = UIColor(Color.wht)
    }
    var body: some View {
        SplashView()
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
