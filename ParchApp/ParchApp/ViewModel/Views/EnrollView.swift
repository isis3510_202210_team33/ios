//
//  EnrollView.swift
//  ParchApp
//
//  Created by Simon Rendon Arango on 23/03/22.
//

import SwiftUI

struct EnrollView: View {
    
    @ObservedObject var global : GlobalModel
    
    var body: some View {
        NavigationView{
            GeometryReader { metrics in
                ZStack{
                    Color.primaryColor.ignoresSafeArea(.all)
                    VStack(spacing:30){
                        
                        Image("Logo").resizable().scaledToFit().frame(width: metrics.size.width*0.5, height: metrics.size.height*0.25, alignment: .center).padding(20)
                            .foregroundColor(Color.white)
                            .minimumScaleFactor(0.01)
                            .shadow(color: Color.black.opacity(0.6), radius: 1, x: 2, y: 2)
                        
                        TitleLogoView().frame( height: 100)
                        
                        Button(action: toSignIn){
                            Text("Sign In")
                                .font(.system(size :25))
                                .fontWeight(.bold)
                                .foregroundColor(Color.blk)
                                .frame(width: metrics.size.width * 0.75, height: metrics.size.height*0.07, alignment: .center)
                                .background(Color.wht)
                                .cornerRadius(100)
                        }
                        
                        Button(action: toSignUp){
                            Text("Sign Up")
                                .font(.system(size :25))
                                .fontWeight(.bold)
                                .foregroundColor(Color.blk)
                                .frame(width: metrics.size.width * 0.75, height: metrics.size.height*0.07, alignment: .center)
                                .background(Color.wht)
                                .cornerRadius(100)
                        }
                    }
                }
            }
        }
    }
    
    func toSignIn(){
        global.loggedIn = true
    }

    func toSignUp(){
        global.loggedIn = true
    }
}

struct EnrollView_Previews: PreviewProvider {
    static var previews: some View {
        EnrollView(global: GlobalModel())
    }
}


