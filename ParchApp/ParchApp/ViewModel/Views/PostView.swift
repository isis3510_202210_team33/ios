//
//  PostView.swift
//  ParchApp
//
//  Created by Simon Rendon Arango on 23/03/22.
//

import SwiftUI

struct PostView: View {
    
    //var reviewUser : UsuarioModel
    //var reviewedPlace : LugarModel
    //var isAd : Bool
    
    @State private var isShareSheetShowing = false
    
    var post : PostModel
        init(post: PostModel) {
            self.post = post
        }
    
    
    func shareButton() {
        
        isShareSheetShowing.toggle()
        
        let url = URL(string: "https://www.youtube.com/watch?v=dQw4w9WgXcQ&ab_channel=RickAstley")
        let av = UIActivityViewController(activityItems: [url!], applicationActivities: nil)

        UIApplication.shared.windows.first?.rootViewController?.present(av, animated: true, completion: nil)
    }
    
    var body: some View {
      GeometryReader{metrics in
            HStack{
                Spacer()
                ZStack{
                    Color.secondaryColor.opacity(0.2)
                    HStack{
                        Spacer()
                        VStack{
                            Image("PersonPlaceholder")
                                .resizable()
                                .scaledToFit()
                                .frame(width: metrics.size.width*0.15, height: metrics.size.width*0.1)
                                .clipShape(Circle())
                                .overlay(Circle().stroke(Color.secondaryColor, lineWidth: 3))
                            
                            Image(systemName: "heart")
                                .resizable()
                                .scaledToFit()
                                .frame(width: metrics.size.width*0.08, height: metrics.size.width*0.08)
                            
                            Button(action: shareButton){
                            Image(systemName: "square.and.arrow.up")
                                .resizable()
                                .scaledToFit()
                                .frame(width: metrics.size.width*0.08, height: metrics.size.width*0.08)
                                
                            }
                            
                        }
                        VStack{
                            Text(post.titulo)
                                .fontWeight(.bold)
                            Text(post.descripcion)
                            Text("@username").foregroundColor(Color.secondaryColor)
                            
                        }.frame(width: metrics.size.width*0.5)
                        VStack{
                            Text("Place Name")
                                .fontWeight(.bold)
                                .minimumScaleFactor(0.01)
                            Image(systemName: "photo")
                                .resizable()
                                .scaledToFit()
                                .frame(width: metrics.size.width*0.18, height: metrics.size.width*0.18)
                            
                            Text("#/10")
                                .foregroundColor(Color.secondaryColor)
                        }
                        Spacer()
                    }
                }.cornerRadius(10)
                
                Spacer()
            }
        }
        
    }
}
