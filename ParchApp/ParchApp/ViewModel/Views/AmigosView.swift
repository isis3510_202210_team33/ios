//
//  AmigosView.swift
//  ParchApp
//
//  Created by Federico Gadea on 7/05/22.
//

import SwiftUI

struct AmigosView: View {
    @State var presentPopup = false
    @State var amigos : [ClienteModel] = []
    @ObservedObject var monitor = NetworkMonitor()
    
    var body: some View {
        GeometryReader{metrics in
            VStack {
                ScrollView {
                        VStack{
                                ForEach(amigos){amigo in
                                    AmigoView (amigo: amigo).frame(width: metrics.size.width, height: metrics.size.height*0.3)
                                }
                        }
                    }
                }
        }.onAppear(perform: {
            if (self.monitor.isConnected){
                let amigos = HTTPRequest()
                amigos.getAmigos() { lista in
                    self.amigos = lista
                
            }}
        })
        
        .alert(isPresented: self.$monitor.notConnected) {
            Alert (title: Text("No Internet Connection"),
                   message: Text("Please enable Wifi or Celluar data"),
                   primaryButton: .default(Text("Settings"), action: {
                UIApplication.shared.open(URL(string: UIApplication.openSettingsURLString)!)
            }),
            secondaryButton: .default(Text("Cancel"))
            )
        }
    }
}

