//
//  MainView.swift
//  ParchApp
//
//  Created by Simon Rendon Arango on 23/03/22.
//

import SwiftUI
import Alamofire

struct MyVariables {
    static var cacheCliente = Cache<String, ClienteModel>()
    static var usDef = UserDefaults.standard
    static var clientEmail = usDef.string(forKey: "email")
    static var cliente = ClienteModel(id: 10000, username: "username", nombre: "aszxcvfbgn", password: "password", correo: "correo", fechaNacimiento: Date.now, telefono: 12345678, grupos: [], amigos: [], favoritos: [], intereses: [])
}
struct MainView: View {
    
    @ObservedObject var monitor = NetworkMonitor()
    
    typealias Handler = (Result<ClienteModel, Error>) -> Void

    let defaults = UserDefaults.standard
    
    
    @ObservedObject var global : GlobalModel
    @State var clientes : [ClienteModel] = []
    
    var body: some View {
        TabView {
            PostsView()
                    .tabItem {
                        Image(systemName: "house.fill")
                        Text("Inicio")
                    }
            MapView()
                    .tabItem {
                        Image(systemName: "map.fill")
                        Text("Mapa")
                }
            MyPlansView()
                .tabItem {
                    Image(systemName: "star.fill")
                    Text("Planes")
                }
            FriendsView()
                .tabItem {
                    Image(systemName: "person.3.fill")
                    Text("Amigos")
                }
            ProfileView(global:global,profilePic: Image("PersonPlaceholder"))
                .tabItem {
                    Image(systemName: "person.fill")
                    Text("Perfil")
                }
            
        }
        .onAppear(perform: {
            
            print("Hey")
            print(monitor.isConnected)
            let email = defaults.string(forKey: "userEmail")
            if (monitor.isConnected){
                print("Hey2")
                DispatchQueue.global(qos: .userInteractive).async {
                   // loadCliente(email: email!)
                    print(MyVariables.cacheCliente[email!] != nil )
                }
                print("Hey3")
            }
        })
        .accentColor(Color.secondaryColor)
        .alert(isPresented: self.$monitor.notConnected) {
            Alert (title: Text("No Internet Connection"),
                   message: Text("Please enable Wifi or Celluar data"),
                   primaryButton: .default(Text("Settings"), action: {
                UIApplication.shared.open(URL(string: UIApplication.openSettingsURLString)!)
            }),
            secondaryButton: .default(Text("Cancel"))
            )
        }
    }
    
    func loadCliente(email: String){
        
        print("load1")
            if MyVariables.cacheCliente[email] != nil {
                let client = MyVariables.cacheCliente.value(forKey: email)!
                print(client)
                MyVariables.cliente = client
                print("was cached")
                print("load2")
              return
            }
        print("load3")
            let cliente = HTTPRequest()
            cliente.getCliente(){lista in
                print("load4")
                self.clientes.append(lista)
                let toCache = self.clientes[0]
                MyVariables.cliente = toCache
                print(MyVariables.cacheCliente.value(forKey: email) as Any)
                MyVariables.cacheCliente.insert(self.clientes[0], forKey: email)
                print(toCache)
                print("was retrieved")
        }
        print("load5")
        
    }
}


struct MainView_Previews: PreviewProvider {
    static var previews: some View {
        MainView(global: GlobalModel())
    }
}
