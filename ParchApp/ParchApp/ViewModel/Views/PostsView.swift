//
//  PostsView.swift
//  ParchApp
//
//  Created by Federico Gadea on 7/05/22.
//

import SwiftUI

struct PostsView: View {
    @State var presentPopup = false
    @State var posts : [PostModel] = []
    @ObservedObject var monitor = NetworkMonitor()
 
    var body: some View {
        GeometryReader{metrics in
            VStack {
                TopBarView().frame(width: metrics.size.width, height: metrics.size.height*0.1)
                ScrollView {
                    VStack{
                        
                            ForEach(posts){post in
                                PostView (post: post).frame(width: metrics.size.width, height: metrics.size.height*0.3)
                            }
                    }
                }
            }
        }.onAppear(perform: {
            if (self.monitor.isConnected){
                let posts = HTTPRequest()
                posts.getPosts() { lista in
                    self.posts = lista
                }
            }
        })
        .alert(isPresented: self.$monitor.notConnected) {
            Alert (title: Text("No Internet Connection"),
                   message: Text("Please enable Wifi or Celluar data"),
                   primaryButton: .default(Text("Settings"), action: {
                UIApplication.shared.open(URL(string: UIApplication.openSettingsURLString)!)
            }),
            secondaryButton: .default(Text("Cancel"))
            )
        }
    }
}
