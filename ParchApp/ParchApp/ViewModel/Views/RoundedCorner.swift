//
//  RoundedCorner.swift
//  ParchApp
//
//  Created by Simon Rendon Arango on 31/03/22.
//

import Foundation
import SwiftUI

struct RoundedCorner: Shape {
    
    

    var radius: CGFloat = .infinity
    var corners: UIRectCorner = .allCorners

    func path(in rect: CGRect) -> Path {
        let path = UIBezierPath(roundedRect: rect, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        return Path(path.cgPath)
    }
}
