//
//  MapView.swift
//  ParchApp
//
//  Created by Federico Gadea on 24/03/22.
//

import SwiftUI
import MapKit

struct MapView: View {
    @StateObject private var viewModel = MapViewModel()
    
    struct Marker: Identifiable {
        let id = UUID()
        var nombre = "test"
        var location: MapMarker
    }
    
    let markers = [Marker(location: MapMarker(coordinate: CLLocationCoordinate2D(latitude: 4.660, longitude: -74.080), tint: .secondaryColor))]
    
    var body: some View {
        
        GeometryReader{metrics in
            VStack {
                
                TopBarView().frame(width: metrics.size.width, height: metrics.size.height*0.1)
                        VStack{
                            Map(coordinateRegion: $viewModel.region, showsUserLocation: true, annotationItems: markers){ marker in
                                marker.location
                            }
                                 .accentColor(Color.primaryColor)
                                 .onAppear{
                                     viewModel.checkIfLocationIsEnable()
                                 }.alert(isPresented: self.$viewModel.alertVisible) {
                                     Alert (title: Text("Location access required"),
                                            message: Text("Go to Settings?"),
                                            primaryButton: .default(Text("Settings"), action: {
                                                UIApplication.shared.open(URL(string: UIApplication.openSettingsURLString)!)
                                                 viewModel.checkIfLocationIsEnable()
                                            }),
                                            secondaryButton: .default(Text("Cancel"))
                                     )
                                 }
                        }
                }
            }
        }
}

struct MapView_Previews: PreviewProvider {
    static var previews: some View {
        MapView()
    }
}

