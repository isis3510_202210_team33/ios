//
//  SignUpView.swift
//  AuthenticationStarter
//
//  Created by Work on 13.12.21.
//

import SwiftUI
import Firebase
import ClockKit
import Contacts

struct SignUpView: View {
    
    @ObservedObject var global = GlobalModel()
    @ObservedObject var monitor = NetworkMonitor()
    
    @EnvironmentObject var viewRouter: ViewRouter
    
    @State var email = ""
    @State var password = ""
    @State var passwordConfirmation = ""
    @State var username = ""
    @State var nombre = ""
    @State var fechaNacimiento = Date()
    @State var telefono = ""
    @State var signUpProcessing = false
    @State var signUpErrorMessage = ""
    let defaults = UserDefaults.standard
    
    var body: some View {
        NavigationView{
            GeometryReader { metrics in
                ZStack{
                    Color.primaryColor.ignoresSafeArea(.all)
        VStack(spacing: 10) {
            
            LogoView()
            ScrollView{
                
            SignUpCredentialFields(email: $email, password: $password, passwordConfirmation: $passwordConfirmation, username: $username, nombre: $nombre, telefono: $telefono, fechaNacimiento: $fechaNacimiento)
            Button(action: {
                
                signUpUser(userEmail: email, userPassword: password, username: username, nombre: nombre, fechaNacimiento: fechaNacimiento, telefono: Int(telefono) ?? 0)
            }) {
                Text("Sign Up")
                    .bold()
                    .frame(width: 360, height: 50)
                    .background(.thinMaterial)
                    .cornerRadius(10)
            }
                .disabled(!signUpProcessing && !email.isEmpty && !password.isEmpty && !passwordConfirmation.isEmpty && password == passwordConfirmation ? false : true)
            if signUpProcessing {
                ProgressView()
            }
            if !signUpErrorMessage.isEmpty {
                Text("Failed creating account: \(signUpErrorMessage)")
                    .foregroundColor(.red)
            }}
            HStack {
                Text("Already have an account?")
                Button(action: {
                    viewRouter.currentPage = .signInPage
                }) {
                    Text("Log In")
                }
            }
                .opacity(0.9)
            
        }
            .padding()
        }
                
            }
            
        }
        
    }
    
    func signUpUser(userEmail: String, userPassword: String, username: String, nombre: String, fechaNacimiento: Date, telefono: Int) {
        
        signUpProcessing = true
        
        Auth.auth().createUser(withEmail: userEmail, password: userPassword) { authResult, error in
            guard error == nil else {
                signUpErrorMessage = error!.localizedDescription
                signUpProcessing = false
                return
            }
            
            switch authResult {
            case .none:
                print("Could not create account.")
                signUpProcessing = false
                
            case .some(_):
                if(monitor.isConnected){
                print("User created")
                signUpProcessing = false
                viewRouter.currentPage = .homePage
                
                let cliente = HTTPRequest()
                cliente.postCliente(username: username, nombre: nombre, password: userPassword, correo: userEmail, fechaNacimiento: fechaNacimiento, telefono: telefono)
                defaults.set(email, forKey: "userEmail")
                    defaults.set(true, forKey: "userLoggedIn")
                    
                }
            }
        }
        
    }
    
}

struct SignUpView_Previews: PreviewProvider {
    static var previews: some View {
        SignUpView()
    }
}

struct LogoView: View {
    var body: some View {
        Image("Logo").resizable().scaledToFit()
            .foregroundColor(Color.white)
            .minimumScaleFactor(0.01)
            .shadow(color: Color.black.opacity(0.6), radius: 1, x: 2, y: 2)
        
        TitleLogoView().frame( height: 100)
    }
}

struct SignUpCredentialFields: View {
    
    @Binding var email: String
    @Binding var password: String
    @Binding var passwordConfirmation: String
    @Binding var username : String
    @Binding var nombre : String
    @Binding var telefono : String
    @Binding var fechaNacimiento : Date
    
    var body: some View {
       
        Group {
            TextField("Email", text: $email)
                .padding()
                .background(.thinMaterial)
                .cornerRadius(10)
                .textInputAutocapitalization(.never)
            TextField("username", text: $username)
                .padding()
                .background(.thinMaterial)
                .cornerRadius(10)
                .textInputAutocapitalization(.never)
            TextField("nombre", text: $nombre)
                .padding()
                .background(.thinMaterial)
                .cornerRadius(10)
                .textInputAutocapitalization(.never)
            DatePicker("Enter your birthday", selection: $fechaNacimiento, in: ...Date(), displayedComponents: .date)
            TextField("telefono", value: $telefono, formatter: NumberFormatter())
                .padding()
                .background(.thinMaterial)
                .cornerRadius(10)
                .textInputAutocapitalization(.never)
            SecureField("Password", text: $password)
                .padding()
                .background(.thinMaterial)
                .cornerRadius(10)
            SecureField("Confirm Password", text: $passwordConfirmation)
                .padding()
                .background(.thinMaterial)
                .cornerRadius(10)
                .border(Color.red, width: passwordConfirmation != password ? 1 : 0)
                .padding(.bottom, 30)
        
            
        }
    }
}

