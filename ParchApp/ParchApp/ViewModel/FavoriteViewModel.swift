//
//  FavoriteViewModel.swift
//  ParchApp
//
//  Created by Federico Gadea on 24/03/22.
//

import Foundation
import CoreLocation
import MapKit

class FavoriteViewModel: NSObject, ObservableObject, CLLocationManagerDelegate{
    
    
    @Published var region = MKCoordinateRegion(center: CLLocationCoordinate2D(latitude: 4.700, longitude:  -74.100),
                                                   span: MKCoordinateSpan(latitudeDelta: 0.05,
                                                                          longitudeDelta: 0.05))
    
    var locationManager: CLLocationManager?
    
    func checkLocation(latitude: Double, longitude: Double){
        region.center.latitude = latitude
        region.center.longitude = longitude
    }
    
    func test(latitud: Double, longitud: Double){
        checkLocation(latitude: latitud, longitude: longitud)
    }
}

