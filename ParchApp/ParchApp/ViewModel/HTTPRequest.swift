//
//  HTTPRequest.swift
//  ParchApp
//
//  Created by Federico Gadea on 31/03/22.
//

import Foundation
import Alamofire
import Firebase
import UIKit

class HTTPRequest: ObservableObject {
    
let user = Auth.auth().currentUser
var client = MyVariables.cliente
    
func getCliente(onSuccess: @escaping (ClienteModel) -> Void){
               let url = URL(string: "http://44.198.195.210:8080/clientes")
               var request = URLRequest(url: url!)
               request.httpMethod = HTTPMethod.get.rawValue
               request.setValue("application/json", forHTTPHeaderField: "Content-Type")
                
    AF.request(request).responseJSON{ response in
                   let gruposHash = response.value as! [[String: Any]]
                   
                   var grupos : [Int]
                   var amigos : [Int]
                   var favoritos : [Int]
                   var intereses : [Int]
                   
                   for hash in gruposHash {
                       let test = hash["fields"] as! [String: Any]
                       grupos = test["grupos"] as! [Int]
                       amigos = test["amigos"] as! [Int]
                       favoritos = test["favoritos"] as! [Int]
                       intereses = test["grupos"] as! [Int]
                       
                       if(test["correo"] as? String == self.user?.email ){
                           let stringDate = test["fechaNacimiento"]! as! String
                           
                           let dateFormatter = DateFormatter()
                           dateFormatter.dateFormat = "yyyy-MM-dd"
                           let date = dateFormatter.date(from: stringDate)
                           
                           self.client = ClienteModel(id: hash["pk"] as! Int, username: test["username"] as! String, nombre: test["nombre"] as! String, password: test["password"] as! String, correo: test["correo"] as! String, fechaNacimiento: date!, telefono: Int(test["telefono"] as! String) ?? 0)
                           self.client.intereses = intereses
                           self.client.grupos = grupos
                           self.client.favoritos = favoritos
                           self.client.amigos = amigos
                       }
                   }
        onSuccess(self.client)
            }
        }
    
func getGrupos(onSuccess: @escaping ([GrupoModel]) -> Void){
           let url = URL(string: "http://44.198.195.210:8080/grupos")
           var request = URLRequest(url: url!)
           request.httpMethod = HTTPMethod.get.rawValue
           request.setValue("application/json", forHTTPHeaderField: "Content-Type")
    
           AF.request(request).responseJSON{ response in
               let gruposHash = response.value as! [[String: Any]]
               var gruposGrupo : [GrupoModel] = []
              
               for hash in gruposHash {
                   let test = hash["fields"] as! [String: Any]
                   if((self.client.grupos?.count)! > 0){
                       
                       for grupos in self.client.grupos! {
                           if(grupos == hash["pk"] as! Int){
                               let grupo = GrupoModel(id: hash["pk"] as! Int, nombre: test["nombre"] as! String, balance: test["balance"] as! Float)
                               gruposGrupo.append(grupo)
                           }
                           }}
               }
               onSuccess(gruposGrupo)
           }
       }

func getPlanes(onSuccess: @escaping ([PlanModel]) -> Void){
           let url = URL(string: "http://44.198.195.210:8080/planes")
           var request = URLRequest(url: url!)
           request.httpMethod = HTTPMethod.get.rawValue
           request.setValue("application/json", forHTTPHeaderField: "Content-Type")

           AF.request(request).responseJSON{ response in
               let planesHash = response.value as! [[String: Any]]
               var grupoPlanes : [PlanModel] = []
               
               if (self.client.correo != self.user?.email) {
               self.getCliente() { cliente in
                   self.client = cliente}
               }
               for hash in planesHash {
                   let test = hash["fields"] as! [String: Any]
                   if((self.client.grupos?.count)! > 0){
                       for grupos in self.client.grupos! {
                           
                           if(grupos == test["grupo"] as! Int){
                               let plan = PlanModel(id: hash["pk"] as! Int, nombre: test["nombre"] as! String, descripcion: test["descripcion"] as! String ,presupuesto: test["presupuesto"] as! Float, horaInicia: test["horaInicia"] as! String, grupo: test["grupo"] as! Int, lugar: test["lugar"] as! Int, evento: test["evento"] as! Int)
                        
                   grupoPlanes.append(plan)
                           }}
                   }
               }
               onSuccess(grupoPlanes)
           }
}
    
    func getLugares(onSuccess: @escaping ([LugarModel]) -> Void){
               let url = URL(string: "http://44.198.195.210:8080/lugares")
               var request = URLRequest(url: url!)
               request.httpMethod = HTTPMethod.get.rawValue
               request.setValue("application/json", forHTTPHeaderField: "Content-Type")

               AF.request(request).responseJSON{ response in
                   let lugaresHash = response.value as! [[String: Any]]
                   var grupoLugares : [LugarModel] = []
                   
                   if (self.client.correo != self.user?.email) {
                   self.getCliente() { cliente in
                       self.client = cliente}
                   }
                   for hash in lugaresHash {
                       let test = hash["fields"] as! [String: Any]
                       if((self.client.favoritos?.count)! > 0){
                           
                           for lugares in self.client.favoritos! {
                               if(lugares == hash["pk"] as! Int){
                                   print("id del lugar")
                                   print(lugares)
                                   print("/////")
                                   let lugar = LugarModel(id: hash["pk"] as! Int, nombre: test["nombre"] as! String, precio: test["precio"] as! Int, direccion: test["direccion"] as! String, tipo: test["tipo"] as! String, horaApertura: test["horaApertura"] as! String, horaCierre: test["horaCierre"] as! String, calificacion: test["calificacion"] as! Double, latitud: test["latitud"] as! String, longitud: test["longitud"] as! String)
                      
                       grupoLugares.append(lugar)
                               }}
                       }}
                   onSuccess(grupoLugares)
               }
           }
    
    func getPosts(onSuccess: @escaping ([PostModel]) -> Void){
               let url = URL(string: "http://44.198.195.210:8080/posts")
               var request = URLRequest(url: url!)
               request.httpMethod = HTTPMethod.get.rawValue
               request.setValue("application/json", forHTTPHeaderField: "Content-Type")

               AF.request(request).responseJSON{ response in
                   let postsHash = response.value as! [[String: Any]]
                   var grupoPosts : [PostModel] = []
                   
                   if (self.client.correo != self.user?.email) {
                   self.getCliente() { cliente in
                       self.client = cliente}
                   }
                   
                   for hash in postsHash {
                       let test = hash["fields"] as! [String: Any]
                       
                       if((self.client.favoritos?.count)! > 0){
                           
                           for lugares in self.client.favoritos! {
                               if(lugares == test["lugar"] as! Int){
                                   let post = PostModel(id: hash["pk"] as! Int, titulo: test["titulo"] as! String, descripcion: test["descripcion"] as! String, calificacion: test["calificacion"] as! Float, anuncio: test["anuncio"] as! Bool, cliente: test["cliente"] as! Int, lugar: test["lugar"] as! Int, representante: test["representante"] as! Int)
                                   grupoPosts.append(post)
                               }}
                       }}
                   onSuccess(grupoPosts)
               }
           }
    
    func getAmigos(onSuccess: @escaping ([ClienteModel]) -> Void){
               let url = URL(string: "http://44.198.195.210:8080/clientes")
               var request = URLRequest(url: url!)
               request.httpMethod = HTTPMethod.get.rawValue
               request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        
               AF.request(request).responseJSON{ response in
                   let amigosHash = response.value as! [[String: Any]]
                   var amigosGrupo : [ClienteModel] = []
                   
                   var grupos : [Int]
                   var amigosS : [Int]
                   var favoritos : [Int]
                   var intereses : [Int]
                   
                   for hash in amigosHash {
                       let test = hash["fields"] as! [String: Any]
                       grupos = test["grupos"] as! [Int]
                       amigosS = test["amigos"] as! [Int]
                       favoritos = test["favoritos"] as! [Int]
                       intereses = test["grupos"] as! [Int]
                       
                       if((self.client.amigos?.count)! > 0){
                           for amigos in self.client.amigos! {
                               if(amigos == hash["pk"] as! Int){
                                   let stringDate = test["fechaNacimiento"]! as! String
                                   
                                   let dateFormatter = DateFormatter()
                                   dateFormatter.dateFormat = "yyyy-MM-dd"
                                   let date = dateFormatter.date(from: stringDate)
                                   
                                  var amigo = ClienteModel(id: hash["pk"] as! Int, username: test["username"] as! String, nombre: test["nombre"] as! String, password: test["password"] as! String, correo: test["correo"] as! String, fechaNacimiento: date!, telefono: Int(test["telefono"] as! String) ?? 0)
                                   amigo.intereses = intereses
                                   amigo.grupos = grupos
                                   amigo.favoritos = favoritos
                                   amigo.amigos = amigosS
                                   
                                   amigosGrupo.append(amigo)
                               }
                               }}
                   }
                   onSuccess(amigosGrupo)
               }
           }

    
    func postCliente (username : String, nombre : String, password : String, correo: String,  fechaNacimiento : Date, telefono : Int){
        
        let test = fechaNacimiento.formatted(.iso8601.dateSeparator(.dash).dateTimeSeparator(.space))
        
        let testing = test.components(separatedBy: " ")
        let newtest = testing[0].components(separatedBy: "-")
        
        let finaltesting = newtest[2] + "-" + newtest[1] + "-" + newtest[0]
        
        print(finaltesting)
        
        let parameters: [String: Any] = [
            "username" : username,
            "nombre" : nombre,
            "password" : password,
            "correo" : correo,
            "fechaNacimiento" : finaltesting,
            "telefono" : telefono,
            "imagen": "https://www.seekpng.com/png/detail/245-2454602_tanni-chand-default-user-image-png.png",
            "grupos" : [],
            "planes" : [],
            "favoritos" : [],
            "intereses" : []
        ]
        AF.request("http://44.198.195.210:8080/clientes", method: .post, parameters: parameters, encoding: JSONEncoding.default)
            .responseString { response in
                print(response)
            }
    }
}
