//
//  File.swift
//  ParchApp
//
//  Created by Federico Gadea on 24/03/22.
//

import Foundation
import CoreLocation
import MapKit

final class MapViewModel: NSObject, ObservableObject, CLLocationManagerDelegate{
    
    @Published var region = MKCoordinateRegion(center: CLLocationCoordinate2D(latitude: 4.707, longitude:  -74.107),
                                                   span: MKCoordinateSpan(latitudeDelta: 0.05,
                                                                          longitudeDelta: 0.05))
    @Published var alertVisible = false
    
    var locationManager: CLLocationManager?
    
    func checkIfLocationIsEnable(){
        if(CLLocationManager.locationServicesEnabled()){
            locationManager = CLLocationManager()
            locationManager!.delegate = self
            
        } else {
            print("show alet")
        }
    }
    
    private func checkLocationAuth(){
        guard locationManager != nil else {
            return
        }
        switch locationManager?.authorizationStatus{
            
        case .some(.denied):
            print("you have denied this app location permission. Go into settings to change it.")
            alertVisible = true
            
        case .some(.restricted):
            print("Location is restricted likely due to parental controls.")
            alertVisible = true
        
        case .some(.notDetermined):
            locationManager?.requestWhenInUseAuthorization()
            
            
        case .some(.authorizedAlways), .some(.authorizedWhenInUse):
            if(locationManager?.location != nil){
            region = MKCoordinateRegion(center: (locationManager?.location!.coordinate)!,
                                        span: MKCoordinateSpan(latitudeDelta: 0.05, longitudeDelta: 0.05))
                alertVisible = false
            }
            else{
                checkIfLocationIsEnable()
            }
        case .none:
            break
        case .some(_):
            break
        }
    }
    
    func locationManagerDidChangeAuthorization(_ manager: CLLocationManager) {
        checkLocationAuth()
    }
}
