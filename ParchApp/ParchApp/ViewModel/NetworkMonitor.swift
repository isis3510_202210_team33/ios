//
//  NetworkMonitor.swift
//  ParchApp
//
//  Created by Federico Gadea on 6/05/22.
//

import Foundation
import Network
 
final class NetworkMonitor: ObservableObject {
    let monitor = NWPathMonitor()
        let queue = DispatchQueue(label: "Monitor")
        
        @Published public var isConnected: Bool = true
        @Published public var notConnected: Bool = false
         
        init() {
            monitor.pathUpdateHandler =  { [weak self] path in
                DispatchQueue.main.async {
                    
                    self?.isConnected = path.status == .satisfied ? true : false
                    self?.notConnected = path.status == .satisfied ? false : true
                }
            }
            monitor.start(queue: queue)
        }
}
