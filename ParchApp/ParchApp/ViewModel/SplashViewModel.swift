//
//  SplashViewModel.swift
//  ParchApp
//
//  Created by Simon Rendon Arango on 31/03/22.
//

import Foundation
import UIKit

class SplashViewModel : ObservableObject {
    @Published var isActive : Bool = false
    @Published var orientation = UIDeviceOrientation.unknown
    
    func activate() {
        isActive = true
    }
    func changeOrientation(newOrientation : UIDeviceOrientation){
        orientation = newOrientation
    }
}
