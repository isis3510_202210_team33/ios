//
//  PostModel.swift
//  ParchApp
//
//  Created by Simon Rendon Arango on 31/03/22.
//

import Foundation

struct PostModel : Identifiable, Codable {
    
    var id : Int
    var titulo : String
    var descripcion : String
    var calificacion : Float
    var anuncio : Bool
    var cliente : Int
    var lugar : Int
    var representante : Int
}
