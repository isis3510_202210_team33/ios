//
//  GrupoModel.swift
//  ParchApp
//
//  Created by Simon Rendon Arango on 31/03/22.
//

import Foundation
import SwiftUI

struct GrupoModel : Identifiable, Codable {
    
    var id : Int
    var nombre : String
    var balance : Float
    
}
