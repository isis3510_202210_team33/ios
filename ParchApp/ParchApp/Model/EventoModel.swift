//
//  EventoModel.swift
//  ParchApp
//
//  Created by Simon Rendon Arango on 31/03/22.
//

import Foundation

struct EventoModel : Identifiable, Codable {
    
    var id : Int
    var nombre : String
    var fecha : Date
    var duracion : Int
    var lugar : Int
    
}
