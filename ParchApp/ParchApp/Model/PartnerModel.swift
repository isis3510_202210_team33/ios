//
//  PartnerModel.swift
//  ParchApp
//
//  Created by Simon Rendon Arango on 31/03/22.
//

import Foundation


struct PartnerModel : Identifiable, Codable {
    
    var id : Int
    var tipoSuscripcion : String
    var fechaFin : Date
    var fechaInicio : Date
    var duracion : Int

}
