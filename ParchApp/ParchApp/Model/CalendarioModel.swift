//
//  CalendarioModel.swift
//  ParchApp
//
//  Created by Simon Rendon Arango on 31/03/22.
//

import Foundation

struct CalendarioModel : Identifiable, Codable {
    
    var id : Int
    var nombre : String
    var cliente : Int
    
}
