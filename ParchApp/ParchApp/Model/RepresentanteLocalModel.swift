//
//  RepresentanteLocalModel.swift
//  ParchApp
//
//  Created by Simon Rendon Arango on 31/03/22.
//

import Foundation

struct RepresentanteLocalModel : Identifiable, Codable {
    
    var id : Int
    var username : String
    var nombre : String
    var password : String
    var correo: String
    var fechaNacimiento : String
    var numeroDocumento : Int
    var tipoDocumento : String
    var cargo : String
    var fechaExpDocumento : Date
    var lugar : Int
    var patner : Int
    
}
