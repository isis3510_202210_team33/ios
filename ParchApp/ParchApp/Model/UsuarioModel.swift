//
//  UsuarioModel.swift
//  ParchApp
//
//  Created by Simon Rendon Arango on 23/03/22.
//

import Foundation

struct UsuarioModel : Identifiable, Codable{
    var id : Int
    var username : String
    var nombre : String
    var password : String
    var correo: String
    var fechaNacimiento : String
}
