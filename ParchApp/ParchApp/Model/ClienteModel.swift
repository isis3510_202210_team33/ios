//
//  ClienteModel.swift
//  ParchApp
//
//  Created by Simon Rendon Arango on 31/03/22.
//

import Foundation

struct ClienteModel : Identifiable, Codable {
    
    var id : Int
    var username : String
    var nombre : String
    var password : String
    var correo: String
    var fechaNacimiento : Date
    var telefono : Int
    var grupos : [Int]?
    var amigos : [Int]?
    var favoritos : [Int]?
    var intereses : [Int]?
                          
}
