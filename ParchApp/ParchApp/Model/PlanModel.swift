//
//  PlanModel.swift
//  ParchApp
//
//  Created by Simon Rendon Arango on 31/03/22.
//

import Foundation

struct PlanModel : Identifiable, Codable {
    
    var id : Int
    var nombre : String
    var descripcion: String
    var presupuesto : Float
    var horaInicia : String
    var grupo : Int
    var lugar : Int
    var evento : Int
    
}
