//
//  InteresModel.swift
//  ParchApp
//
//  Created by Simon Rendon Arango on 31/03/22.
//

import Foundation

struct InteresModel : Identifiable, Codable {
    
    var id : Int
    var tipo : String
    
}
