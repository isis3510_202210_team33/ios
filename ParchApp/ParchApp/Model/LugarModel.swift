//
//  LugarModel.swift
//  ParchApp
//
//  Created by Simon Rendon Arango on 31/03/22.
//

import Foundation

struct LugarModel : Identifiable {
    
    var id : Int
    var nombre : String
    var precio : Int
    var direccion : String
    var tipo : String
    var horaApertura : String
    var horaCierre : String
    var calificacion : Double
    var latitud : String
    var longitud : String
    
}
