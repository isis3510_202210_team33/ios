//
//  ParchAppApp.swift
//  ParchApp
//
//  Created by Simon Rendon Arango on 22/03/22.
//

import SwiftUI
import Firebase

struct DeviceRotationViewModifier: ViewModifier {
    let action: (UIDeviceOrientation) -> Void

    func body(content: Content) -> some View {
        content
            .onAppear()
            .onReceive(NotificationCenter.default.publisher(for: UIDevice.orientationDidChangeNotification)) { _ in
                action(UIDevice.current.orientation)
            }
    }
}
@main
struct ParchAppApp: App {
   
   
    @StateObject var viewRouter = ViewRouter()
        
        init() {
            FirebaseApp.configure()
        }
        
        var body: some Scene {
            WindowGroup {
                SplashView().environmentObject(viewRouter)
            }
        }
    
}
